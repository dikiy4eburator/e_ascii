# e_ascii  Copyright (C) 2020  Wladimir Frank

from queue import Queue
from threading import Thread
from time import sleep

class Color:
    """
    class that provides some default color defenitions,
    and functions to convert RGB or HEX to shell color code.

    """

    # MONOKAI color def
    monokai_color = [
        "101010",
        "960050",
        "66aa11",
        "c47f2c",
        "30309b",
        "7e40a5",
        "3579a8",
        "9999aa",
        "303030",
        "ff0090",
        "80ff00",
        "ffba68",
        "5f5fee",
        "bb88dd",
        "4eb4fa",
        "d0d0d0"
    ]

    ENDC = '\033[0m'
    BRIGHT = '\033[1m'
    DIM = '\033[2m'
    ITALIC = '\033[3m'
    UNDERLINE = '\033[4m'
    BLINK = '\033[5m'
    REVERSE = '\033[7m'
    HIDDEN = '\033[8m'

    WHITE_f = '\033[0;38;2;255;255;255m'
    WHITE_b = '\033[0;48;2;255;255;255m'

    BLACK_f = '\033[0;38;2;0;0;0m'
    BLACK_b = '\033[0;48;2;0;0;0m'

    def rgb_to_color(self, z, M, R, G, B):
        """
        arguments:  z - forgound "f", backround "b"
                    M - Mode expets an int representing the mode from 0 to 7
                    R - red
                    G - dreen
                    B - blue
        """

        M = str(M)

        if z == "f":
            return '\033[' + str(M) + ';38;2;' + str(R) + ';' + str(G) + ';' + str(B) + 'm'

        elif z == "b":
            return '\033[' + str(M) + ';48;2;' + str(R) + ';' + str(G) + ';' + str(B) + 'm'

    def hex_to_color(self, z, M, hexa_code):
        """
        arguments:  z - forgound "f", backround "b"
                    M - Mode expets an int() representing the mode from 0 to 7
                    hexa_code - hexadecimal code with out "#" exsamp.: c47f2c
        """

        R = int(hexa_code[:2], 16)
        G = int(hexa_code[2:4], 16)
        B = int(hexa_code[4:], 16)

        if z == "f":
            return '\033[' + str(M) + ';38;2;' + str(R) + ';' + str(G) + ';' + str(B) + 'm'

        elif z == "b":
            return '\033[' + str(M) + ';48;2;' + str(R) + ';' + str(G) + ';' + str(B) + 'm'


class Tile:
    """
    Objekt that holdes the information of one tile in the Frame Buffer
    """

    def __init__(self, symbol=" ", color_slot_1=Color.ENDC, color_slot_2=Color.ENDC):

        self.symbol = symbol
        self.color_slot_1 = color_slot_1
        self.color_slot_2 = color_slot_2


class Key_event:
    """
    provides keyboard reader that returns key presses
    throu a Queue named key_buffer. normal keys get returned
    as char, special keys as strings in form of "KEY_ESCAPE" or "KEY_ENTER"
    """

    def __init__(self):

        self.key_buffer = Queue()   # Chanel OUT
        self.tunel = Queue()        # for INTERNAL use ONLY

        # STDIN reader "thrower"
        self.input_thread = Thread(target=self.werfer, args=(self.tunel,))
        self.input_thread.daemon = True
        self.input_thread.start()

        # TRANSLATER "Cetcher"
        self.input_thread = Thread(target=self.faenger, args=(self.tunel, self.key_buffer,))
        self.input_thread.daemon = True
        self.input_thread.start()

    def werfer(self, tunel):
        from sys import stdin
        from tty import setcbreak
        from termios import tcgetattr, tcsetattr, TCSADRAIN

        while True:
            fd = stdin.fileno()
            old = tcgetattr(fd)
            try:
                setcbreak(fd)
                tunel.put(ord(stdin.read(1)))
            finally:
                tcsetattr(fd, TCSADRAIN, old)

            # sleep(0.001)

    def faenger(self, tunel, key_buffer):
        simple_key = int()
        complex_key_3 = [int(), int(), int()]
        complex_key_4 = [int(), int(), int(), int()]
        complex_key_5 = [int(), int(), int(), int(), int()]

        while True:
            if not tunel.empty():
                i = tunel.qsize()
                if i == 1:
                    for x in range(i):
                        simple_key = tunel.get()
                    
                    if simple_key == 27:
                        key_buffer.put("KEY_ESCAPE")
                    elif simple_key == 10:
                        key_buffer.put("KEY_ENTER")
                    elif simple_key == 127:
                        key_buffer.put("KEY_BACKSPACE")
                    elif simple_key == 9:
                        key_buffer.put("KEY_TAB")
                    # elif simple_key == 32:
                    #     key_buffer.put("KEY_SPACE")
                    else:
                        key_buffer.put(chr(simple_key))

                elif i == 3:
                    for x in range(i):
                        complex_key_3[x] = tunel.get()
                    
                    if complex_key_3 == [27, 91, 65]:
                        key_buffer.put("KEY_UP")
                    elif complex_key_3 == [27, 91, 68]:
                        key_buffer.put("KEY_LEFT")
                    elif complex_key_3 == [27, 91, 67]:
                        key_buffer.put("KEY_RIGHT")
                    elif complex_key_3 == [27, 91, 66]:
                        key_buffer.put("KEY_DOWN")
                    elif complex_key_3 == [27, 91, 70]:
                        key_buffer.put("KEY_END")
                    elif complex_key_3 == [27, 91, 72]:
                        key_buffer.put("KEY_POS1")
                    elif complex_key_3 == [27, 91, 70]:
                        key_buffer.put("KEY_POS1")

                    # ================================= #

                    elif complex_key_3 == [27, 79, 80]:
                        key_buffer.put("KEY_F1")
                    elif complex_key_3 == [27, 79, 81]:
                        key_buffer.put("KEY_F2")
                    elif complex_key_3 == [27, 79, 82]:
                        key_buffer.put("KEY_F3")
                    elif complex_key_3 == [27, 79, 83]:
                        key_buffer.put("KEY_F4")

                elif i == 4:
                    for x in range(i):
                        complex_key_4[x] = tunel.get()
                    
                    if complex_key_4 == [27, 91, 51, 126]:
                        key_buffer.put("KEY_ENTF")
                    elif complex_key_4 == [27, 91, 50, 126]:
                        key_buffer.put("KEY_EINFG")
                    elif complex_key_4 == [27, 91, 53, 126]:
                        key_buffer.put("KEY_PAGE_UP")
                    elif complex_key_4 == [27, 91, 54, 126]:
                        key_buffer.put("KEY_PAGE_DOWN")

                elif i == 5:
                    for x in range(i):
                        complex_key_5[x] = tunel.get()
                    
                    if complex_key_3 == [27, 91, 49, 53, 126]:
                        key_buffer.put("KEY_F5")
                    elif complex_key_3 == [27, 91, 49, 55, 126]:
                        key_buffer.put("KEY_F6")
                    elif complex_key_3 == [27, 91, 49, 56, 126]:
                        key_buffer.put("KEY_F7")
                    elif complex_key_3 == [27, 91, 49, 57, 126]:
                        key_buffer.put("KEY_F8")
                    elif complex_key_3 == [27, 91, 50, 48, 126]:
                        key_buffer.put("KEY_F9")
                    elif complex_key_3 == [27, 91, 50, 49, 126]:
                        key_buffer.put("KEY_F10")
                    elif complex_key_3 == [27, 91, 50, 51, 126]:
                        key_buffer.put("KEY_F11")
                    elif complex_key_3 == [27, 91, 50, 52, 126]:
                        key_buffer.put("KEY_F12")

            sleep(0.001)
    
    # # BLESSED WAY
    # def get_key_stroke_3(self, key_buffer):

    #     # =====================================
    #     from blessed import Terminal

    #     term = Terminal()
    #     while True:
    #         with term.cbreak(), term.hidden_cursor():
    #             key_buffer.put(term.inkey())
    #     # =====================================
