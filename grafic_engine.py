# e_ascii  Copyright (C) 2020  Wladimir Frank

from subprocess import check_output
from queue import Queue
from time import sleep
from time import time

from .helper import Color, Tile, Key_event
from .drawer import draw_char

from subprocess import call



class grafik_engine:

    def __init__(self):

        self.x_max = int(check_output(["tput", "cols"])) + 1
        self.y_max = int(check_output(["tput", "lines"])) + 1

        self.canvas_x = [1, self.x_max - 1]
        self.canvas_y = [1, self.y_max - 1]

        # ENGINE FLAGS
        self.main_loop_run = True
        self.FPS = 30

        self.pause = False
        self.redraw = False
        self.detour = [False, None]     # if the input should be redirektet to a "noblock reader"

        self.create_frame_buffer()
        self.clear_frame_buffer()
        self.fb_generator = [ [x for x in y] for y in self.FB ]

        self.draw_list = list()
        self.draw_list_generator = [obj for obj in self.draw_list]
        self.draw_list_old = list()

        # Color converter
        self.color_conv = Color()

        # Keboard reader obj
        self.key_buffer = Queue()
        self.key_serv = Key_event()

    def create_frame_buffer(self):

        self.FB = [[Tile(" ") for x in range(self.x_max)] for y in range(self.y_max)]

    def clear_frame_buffer(self):
        for y in range(0, len(self.FB)):
            for x in range(0, len(self.FB[0])):
                self.FB[y][x] = Tile(" ")

        self.fb_generator = [ [x for x in y] for y in self.FB ]

    def refresh_draw_list(self):
        self.clear_frame_buffer()
        self.draw_list_generator = [obj for obj in self.draw_list]
        for drawer in self.draw_list_generator:
            drawer.draw()

    def de_tour(self):
        # Cheks if reader objekt needs extra processing,
        # if not it sends the input to the output key_buffer of the engine
        if not self.key_serv.key_buffer.empty():
            _ = self.key_serv.key_buffer.get()
            if self.detour[0]:
                for x in self.draw_list:
                    if x == self.detour[1]:
                        self.key_buffer.put(_)
                        x.process()
            else:
                self.key_buffer.put(_)

    def add_drawer(self, drawer_class):
        self.draw_list.append(drawer_class)
        return self.draw_list[
            self.draw_list.index(drawer_class)
        ]

    def remove_drawer(self, obj_referenze):
        for i, x in enumerate(self.draw_list):
            if x == obj_referenze:
                self.draw_list[i].pop()

    def draw_frame_buffer(self):
        # keep at least 1 objekt in list to
        # prevent out of range error
        if len(self.draw_list) == 0:
            self.add_drawer(draw_char(self, char=" ", start_vektor=[0, 0]))

        # Key input Detour for non blocking reader
        self.de_tour()

        # refrech draw list
        if self.draw_list != self.draw_list_old or self.redraw is True:
            self.refresh_draw_list()
            self.redraw = False

        for drawer in self.draw_list_generator:
            if hasattr(drawer, "kind") and drawer.kind == "animation":
                drawer.draw()

        # ###########################################################

        print("{}{};{}f".format("\x1b[", 0, 0))

        for y in range(self.y_max):
            for x in range(self.x_max):
                # move cursor
                print("\x1b[{};{}f".format(y, x), end="")
                # set color
                print(f"{self.FB[y][x].color_slot_2 + self.FB[y][x].color_slot_1}", end="")
                # print char to terminal
                print(f"{self.FB[y][x].symbol}", end="")

        self.draw_list_old = self.draw_list

    # ###
    # ###
    # ###
    # ###############
    # ################################
    # #################################################################
    # #####################################################################################################################

    def main_loop(self):
        # HIDE PROMPT
        call("tput civis".split(" "))

        while self.main_loop_run:

            frame_start = time()

            self.draw_frame_buffer()

            # FPS KONTROLER
            frame_time = time() - frame_start
            if frame_time < 1 / self.FPS:
                sleep(1 / self.FPS - frame_time)

        # SHOW PROMPT AND CLEAN THE SHELL
        call("tput cnorm".split(" "))
        call("tput init".split(" "))

    # #####################################################################################################################
    # #################################################################
    # ################################
    # ###############
    # ###
    # ###
    # ###




# NOTES
# ─ │ ┌ ┐ └ ┘ ├ ┤ ┬ ┴ ┼  ← ↑ → ↓ ↔ ↕ ©
