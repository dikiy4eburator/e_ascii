# e_ascii  Copyright (C) 2020  Wladimir Frank

from random import randrange, choice
from .helper import Color, Tile

# #####################################################################
# ###     Animated     ################################################
# #####################################################################


class draw_noblock_work_indicator:

    def __init__(self, engin_obj, vector, color_slot_1=Color.ENDC, color_slot_2=Color.ENDC):

        self.engine = engin_obj
        self.kind = "animation"

        self.count = randrange(0, 9)

        self.start_x = vector[0]
        self.start_y = vector[1]

        self.color_slot_1 = color_slot_1
        self.color_slot_2 = color_slot_2

        self.draw()

    def draw(self):

        # DO NOT draw outside FB
        if (
            self.start_y > 0 and
            self.start_y < self.engine.y_max and
            self.start_x > 0 and
            self.start_x < self.engine.x_max
        ):

            self.engine.FB[self.start_y][self.start_x] = Tile(
                self.count,
                self.color_slot_1,
                self.color_slot_2
            )

            self.count += 1
            if self.count > 9:
                self.count = 0

        return


class draw_WI_matrix:

    def __init__(self, engin_obj, vector, matrix, color_slot_1=Color.ENDC, color_slot_2=Color.ENDC):

        self.engine = engin_obj
        self.kind = "animation"

        self.start_x = vector[0]
        self.start_y = vector[1]

        self.color_slot_1 = color_slot_1
        self.color_slot_2 = color_slot_2

        self.matrix = matrix
        ########################################################################
        ####    Code from -> https://github.com/jsbueno/terminal_matrix    #####
        ########################################################################
        def getchars(start, end): return [chr(i) for i in range(start, end)]

        latin = getchars(0x30, 0x80)
        greek = getchars(0x390, 0x3d0)
        hebrew = getchars(0x5d0, 0x5eb)
        cyrillic = getchars(0x400, 0x50)

        self.chars = latin + greek + hebrew + cyrillic
        ########################################################################
        ########################################################################

        self.field = [["" for x in range(self.matrix[0])] for y in range(self.matrix[1])]

        self.draw()
    
    def draw(self):

        for y in range(self.matrix[1]):
            for x in range(self.matrix[0]):

                # DO NOT draw outside FB
                if (
                    self.start_y + y > 0 and
                    self.start_y + y < self.engine.y_max and
                    self.start_x + x > 0 and
                    self.start_x + x < self.engine.x_max
                ):

                    _ = randrange(0, 100)
                    if _ > 60:
                        self.engine.FB[self.start_y + y][self.start_x + x] = Tile(
                            f"{choice(self.chars)}",
                            self.color_slot_1,
                            self.color_slot_2
                        )
                    else:
                        pass
        return
