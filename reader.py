# e_ascii  Copyright (C) 2020  Wladimir Frank

from .helper import Color, Tile

# #####################################################################
# ###     NON BLOCKING READER     #####################################
# #####################################################################


# INPUT CLASSES
class draw_noblock_input:
    """
    posible parameters are:
        - start_vektor      <---- list(),  [x, y]
        - color_slot_1      <---- str(),   forgrund collor in form of shell string
        - color_slot_2      <---- str(),   backgrund collor in form of shell string
    """

    def __init__(self, engine, **parameter):

        self.engin_obj = engine
        self.com_chanel = engine.key_buffer
        self.kind = "reader"
        
        self.start_x = 1
        self.start_y = 1

        self.cursor_pos = 0
        self.text_befor_cursor = str()
        self.text_after_cursor = str()
        self.color_slot_1 = Color.ENDC
        self.color_slot_2 = Color.ENDC

        self.result = str()
        self.state_list = ["WIP", "ABORT", "DONE"]
        self.state = self.state_list[0]
        
        self.engin_obj.detour[0], self.engin_obj.detour[1] = True, self

        for entry in parameter:
            if "start_vektor" in entry:
                self.start_x = parameter['start_vektor'][0]
                self.start_y = parameter['start_vektor'][1]

            elif "color_slot_1" in entry:
                 self.color_slot_1 = parameter['color_slot_1']

            elif "color_slot_2" in entry:
                 self.color_slot_2 = parameter['color_slot_2']

            else:
                pass

        self.draw()

    def change_parameter(self,  **parameter):
        for entry in parameter:
            if "start_vektor" in entry:
                self.start_x = parameter['start_vektor'][0]
                self.start_y = parameter['start_vektor'][1]

            elif "color_slot_1" in entry:
                 self.color_slot_1 = parameter['color_slot_1']

            elif "color_slot_2" in entry:
                 self.color_slot_2 = parameter['color_slot_2']

            else:
                pass
        
        self.draw()
        
    def process(self):

        input_read = self.com_chanel.get()

        if input_read == "KEY_ESCAPE":
            self.state = self.state_list[1]
            self.engin_obj.detour = [False, None]

        elif input_read == "KEY_ENTER":
            self.result = self.text_befor_cursor + self.text_after_cursor
            self.state = self.state_list[2]
            self.engin_obj.detour = [False, None]

        elif input_read == "KEY_LEFT":
            if len(self.text_befor_cursor) > 0:
                self.text_after_cursor = self.text_befor_cursor[-1] + self.text_after_cursor
                self.text_befor_cursor = self.text_befor_cursor[:-1]

            self.engin_obj.redraw = True
            self.draw()

        elif input_read == "KEY_RIGHT":
            if len(self.text_after_cursor) > 0:
                self.text_befor_cursor += self.text_after_cursor[0]
                self.text_after_cursor = self.text_after_cursor[1:]

            self.engin_obj.redraw = True
            self.draw()
        
        elif input_read == "KEY_ENTF":
            if len(self.text_after_cursor) > 0:
                self.text_after_cursor = self.text_after_cursor[1:]

            self.engin_obj.redraw = True
            self.draw()

        elif input_read == "KEY_BACKSPACE":
            if self.cursor_pos < len(self.text_befor_cursor) + len(self.text_after_cursor):
                self.text_befor_cursor = self.text_befor_cursor[0:-1]
            else:
                pass

            self.engin_obj.redraw = True
            self.draw()

        else:
            self.text_befor_cursor += input_read

            self.engin_obj.redraw = True
            self.draw()

    def draw(self):

        # print text before cursor
        for i in range(len(self.text_befor_cursor)):
            # DO NOT draw outside FB
            if (
                self.start_y > 0 and
                self.start_y < self.engin_obj.y_max and
                self.start_x + i > 0 and
                self.start_x + i < self.engin_obj.x_max
            ):

                self.engin_obj.FB[self.start_y][i + self.start_x] = Tile(
                    self.text_befor_cursor[i],
                    self.color_slot_1,
                    self.color_slot_2
                )

        # print curser
        if (
            self.start_y > 0 and
            self.start_y < self.engin_obj.y_max and
            self.start_x + len(self.text_befor_cursor) > 0 and
            self.start_x + len(self.text_befor_cursor) < self.engin_obj.x_max
        ):

            self.engin_obj.FB[self.start_y][self.start_x + len(self.text_befor_cursor)] = Tile(
                "|",
                self.color_slot_1,
                self.color_slot_2
            )

        # remove curser from last position after deletion of a char
        if (
            self.start_y > 0 and
            self.start_y < self.engin_obj.y_max and
            self.start_x + len(self.text_befor_cursor) + 1 > 0 and
            self.start_x + len(self.text_befor_cursor) + 1 < self.engin_obj.x_max
        ):

            self.engin_obj.FB[self.start_y][self.start_x + len(self.text_befor_cursor) + 1] = Tile(
                " ",
                self.color_slot_1,
                self.color_slot_2
            )

        # print text after cursor
        for i in range(len(self.text_after_cursor)):
            # DO NOT draw outside FB
            if (
                self.start_y > 0 and
                self.start_y < self.engin_obj.y_max and
                self.start_x + len(self.text_befor_cursor) + 1 + i > 0 and
                self.start_x + len(self.text_befor_cursor) + 1 + i < self.engin_obj.x_max
            ):

                self.engin_obj.FB[self.start_y][self.start_x + len(self.text_befor_cursor) + 1 + i] = Tile(
                    self.text_after_cursor[i],
                    self.color_slot_1,
                    self.color_slot_2
                )


class draw_noblock_input_middle(draw_noblock_input):

    def draw(self):

        str_leng = int((len(self.text_befor_cursor) + len(self.text_after_cursor)) / 2)

        # print bufferet text to the framebuffer
        for i in range(len(self.text_befor_cursor)):
            # DO NOT draw outside FB
            if (
                self.start_y > 0 and
                self.start_y < self.engin_obj.y_max and
                self.start_x - str_leng + i > 0 and
                self.start_x - str_leng + i < self.engin_obj.x_max
            ):

                self.engin_obj.FB[self.start_y][self.start_x - str_leng + i] = Tile(
                    self.text_befor_cursor[i],
                    self.color_slot_1,
                    self.color_slot_2
                )

        # print curser
        # DO NOT draw outside FB
        if (
            self.start_y > 0 and
            self.start_y < self.engin_obj.y_max and
            self.start_x - str_leng + len(self.text_befor_cursor) > 0 and
            self.start_x - str_leng + len(self.text_befor_cursor) < self.engin_obj.x_max
        ):

            self.engin_obj.FB[self.start_y][self.start_x - str_leng + len(self.text_befor_cursor)] = Tile(
                "|",
                self.color_slot_1,
                self.color_slot_2
            )

        # remove curser from last position after deletion of a char
        # DO NOT draw outside FB
        if (
            self.start_y > 0 and
            self.start_y < self.engin_obj.y_max and
            self.start_x - str_leng + len(self.text_befor_cursor) + 1 > 0 and
            self.start_x - str_leng + len(self.text_befor_cursor) + 1 < self.engin_obj.x_max
        ):
            self.engin_obj.FB[self.start_y][self.start_x - str_leng + len(self.text_befor_cursor) + 1] = Tile(
                " ",
                self.color_slot_1,
                self.color_slot_2
            )

        for i in range(len(self.text_after_cursor)):
            # DO NOT draw outside FB
            if (
                self.start_y > 0 and
                self.start_y < self.engin_obj.y_max and
                self.start_x - str_leng + len(self.text_befor_cursor) + 1 + i > 0 and
                self.start_x - str_leng + len(self.text_befor_cursor) + 1 + i < self.engin_obj.x_max
            ):
                self.engin_obj.FB[self.start_y][self.start_x - str_leng + len(self.text_befor_cursor) + 1 + i] = Tile(
                    self.text_after_cursor[i],
                    self.color_slot_1,
                    self.color_slot_2
                )


class draw_noblock_input_right(draw_noblock_input):

    def draw(self):

        str_leng = len(self.text_befor_cursor) + len(self.text_after_cursor)

        # print bufferet text to the framebuffer
        for i in range(len(self.text_befor_cursor)):
            # DO NOT draw outside FB
            if (
                self.start_y > 0 and
                self.start_y < self.engin_obj.y_max and
                self.start_x - str_leng + i > 0 and
                self.start_x - str_leng + i < self.engin_obj.x_max
            ):

                self.engin_obj.FB[self.start_y][self.start_x - str_leng + i] = Tile(
                    self.text_befor_cursor[i],
                    self.color_slot_1,
                    self.color_slot_2
                )

        # print curser
        # DO NOT draw outside FB
        if (
            self.start_y > 0 and
            self.start_y < self.engin_obj.y_max and
            self.start_x - str_leng + len(self.text_befor_cursor) > 0 and
            self.start_x - str_leng + len(self.text_befor_cursor) < self.engin_obj.x_max
        ):

            self.engin_obj.FB[self.start_y][self.start_x - str_leng + len(self.text_befor_cursor)] = Tile(
                "|",
                self.color_slot_1,
                self.color_slot_2
            )

        # remove curser from last position after deletion of a char
        # DO NOT draw outside FB
        if (
            self.start_y > 0 and
            self.start_y < self.engin_obj.y_max and
            self.start_x - str_leng + len(self.text_befor_cursor) + 1 > 0 and
            self.start_x - str_leng + len(self.text_befor_cursor) + 1 < self.engin_obj.x_max
        ):
            self.engin_obj.FB[self.start_y][self.start_x - str_leng + len(self.text_befor_cursor) + 1] = Tile(
                " ",
                self.color_slot_1,
                self.color_slot_2
            )

        for i in range(len(self.text_after_cursor)):
            # DO NOT draw outside FB
            if (
                self.start_y > 0 and
                self.start_y < self.engin_obj.y_max and
                self.start_x - str_leng + len(self.text_befor_cursor) + 1 + i > 0 and
                self.start_x - str_leng + len(self.text_befor_cursor) + 1 + i < self.engin_obj.x_max
            ):
                self.engin_obj.FB[self.start_y][self.start_x - str_leng + len(self.text_befor_cursor) + 1 + i] = Tile(
                    self.text_after_cursor[i],
                    self.color_slot_1,
                    self.color_slot_2
                )


# OBSCURED INPUT CLASSES
class draw_noblock_input_obscure:

    """
    posible parameters are:
        - start_vektor      <---- list(),  [x, y]
        - color_slot_1      <---- str(),   forgrund collor in form of shell string
        - color_slot_2      <---- str(),   backgrund collor in form of shell string
    """

    def __init__(self, engine, **parameter):

        self.engin_obj = engine
        self.com_chanel = engine.key_buffer
        self.kind = "reader"
        
        self.start_x = 1
        self.start_y = 1

        self.cursor_pos = 0
        self.text_befor_cursor = str()
        self.text_after_cursor = str()
        self.color_slot_1 = Color.ENDC
        self.color_slot_2 = Color.ENDC

        self.result = str()
        self.state_list = ["WIP", "ABORT", "DONE"]
        self.state = self.state_list[0]
        self.text = str()
        self.text_obscure = str()

        self.engin_obj.detour[0], self.engin_obj.detour[1] = True, self

        for entry in parameter:
            if "start_vektor" in entry:
                self.start_x = parameter['start_vektor'][0]
                self.start_y = parameter['start_vektor'][1]

            elif "color_slot_1" in entry:
                 self.color_slot_1 = parameter['color_slot_1']

            elif "color_slot_2" in entry:
                 self.color_slot_2 = parameter['color_slot_2']

            else:
                pass

        self.draw()

    def change_parameter(self,  **parameter):
        for entry in parameter:
            if "start_vektor" in entry:
                self.start_x = parameter['start_vektor'][0]
                self.start_y = parameter['start_vektor'][1]

            elif "color_slot_1" in entry:
                 self.color_slot_1 = parameter['color_slot_1']

            elif "color_slot_2" in entry:
                 self.color_slot_2 = parameter['color_slot_2']

            else:
                pass

        self.draw()
        
    def process(self):

        input_read = self.com_chanel.get()

        if input_read == "KEY_ESCAPE":
            self.state = self.state_list[1]
            self.engin_obj.detour = [False, None]

        elif input_read == "KEY_ENTER":
            self.result = self.text
            self.state = self.state_list[2]
            self.engin_obj.detour = [False, None]

        elif input_read == "KEY_BACKSPACE":
            self.text = self.text[0:-1]
            self.text_obscure = self.text_obscure[0:-1]

            self.engin_obj.redraw = True
            self.draw()

        elif input_read == "KEY_LEFT":
            pass

        elif input_read == "KEY_RIGHT":
            pass

        elif input_read == "KEY_ENTF":
            pass

        else:
            self.text += input_read
            self.text_obscure += "*"

            self.engin_obj.redraw = True
            self.draw()
      
    def draw(self):

        for i in range(len(self.text)):
            # DO NOT draw outside FB
            if (
                self.start_y > 0 and
                self.start_y < self.engin_obj.y_max and
                self.start_x + i > 0 and
                self.start_x + i < self.engin_obj.x_max
            ):
                self.engin_obj.FB[self.start_y][self.start_x + i] = Tile(
                    self.text_obscure[i] + "|",
                    self.color_slot_1,
                    self.color_slot_2
                )

        # DO NOT draw outside FB
        if (
            self.start_y > 0 and
            self.start_y < self.engin_obj.y_max and
            self.start_x + len(self.text) > 0 and
            self.start_x + len(self.text) < self.engin_obj.x_max
        ):
            self.engin_obj.FB[self.start_y][self.start_x + len(self.text)] = Tile(
                "|",
                self.color_slot_1,
                self.color_slot_2
            )

        # DO NOT draw outside FB
        if (
            self.start_y > 0 and
            self.start_y < self.engin_obj.y_max and
            self.start_x + len(self.text) + 1 > 0 and
            self.start_x + len(self.text) + 1 < self.engin_obj.x_max
        ):
            self.engin_obj.FB[self.start_y][self.start_x + len(self.text) + 1] = Tile(
                " ",
                self.color_slot_1,
                self.color_slot_2
            )


class draw_noblock_input_obscure_middle(draw_noblock_input_obscure):

    def draw(self):

        for i in range(len(self.text)):
            # DO NOT draw outside FB
            if (
                self.start_y > 0 and
                self.start_y < self.engin_obj.y_max and
                self.start_x - int(len(self.text) / 2) + i > 0 and
                self.start_x - int(len(self.text) / 2) + i < self.engin_obj.x_max
            ):
                self.engin_obj.FB[self.start_y][self.start_x - int(len(self.text) / 2) + i] = Tile(
                    self.text_obscure[i] + "|",
                    self.color_slot_1,
                    self.color_slot_2
                )

        # DO NOT draw outside FB
        if (
            self.start_y > 0 and
            self.start_y < self.engin_obj.y_max and
            self.start_x - int(len(self.text) / 2) + len(self.text) > 0 and
            self.start_x - int(len(self.text) / 2) + len(self.text) < self.engin_obj.x_max
        ):
            self.engin_obj.FB[self.start_y][self.start_x - int(len(self.text) / 2) + len(self.text)] = Tile(
                "|",
                self.color_slot_1,
                self.color_slot_2
            )

        # DO NOT draw outside FB
        if (
            self.start_y > 0 and
            self.start_y < self.engin_obj.y_max and
            self.start_x - int(len(self.text) / 2) + len(self.text) + 1 > 0 and
            self.start_x - int(len(self.text) / 2) + len(self.text) + 1 < self.engin_obj.x_max
        ):
            self.engin_obj.FB[self.start_y][self.start_x - int(len(self.text) / 2) + len(self.text) + 1] = Tile(
                " ",
                self.color_slot_1,
                self.color_slot_2
            )


class draw_noblock_input_obscure_right(draw_noblock_input_obscure):

    def draw(self):

        for i in range(len(self.text)):
            # DO NOT draw outside FB
            if (
                self.start_y > 0 and
                self.start_y < self.engin_obj.y_max and
                self.start_x - int(len(self.text)) + i > 0 and
                self.start_x - int(len(self.text)) + i < self.engin_obj.x_max
            ):
                self.engin_obj.FB[self.start_y][self.start_x - int(len(self.text)) + i] = Tile(
                    self.text_obscure[i] + "|",
                    self.color_slot_1,
                    self.color_slot_2
                )

        # DO NOT draw outside FB
        if (
            self.start_y > 0 and
            self.start_y < self.engin_obj.y_max and
            self.start_x - int(len(self.text)) + len(self.text) > 0 and
            self.start_x - int(len(self.text)) + len(self.text) < self.engin_obj.x_max
        ):
            self.engin_obj.FB[self.start_y][self.start_x - int(len(self.text)) + len(self.text)] = Tile(
                "|",
                self.color_slot_1,
                self.color_slot_2
            )

        # DO NOT draw outside FB
        if (
            self.start_y > 0 and
            self.start_y < self.engin_obj.y_max and
            self.start_x - int(len(self.text)) + len(self.text) + 1 > 0 and
            self.start_x - int(len(self.text)) + len(self.text) + 1 < self.engin_obj.x_max
        ):
            self.engin_obj.FB[self.start_y][self.start_x - int(len(self.text)) + len(self.text) + 1] = Tile(
                " ",
                self.color_slot_1,
                self.color_slot_2
            )


# MENU CLASSES
class draw_noblock_menu:
    """
    posible parameters are:
        - menu_lengh        <---- int(),    menu lengh in lines
        - start_vektor      <---- list(),   [x, y]
        - color_slot_1      <---- str(),    forgrund collor in form of shell string
        - color_slot_2      <---- str(),    backgrund collor in form of shell string
    """

    def __init__(self, engin_obj, menu_list, **parameter):

        self.engin_obj = engin_obj
        self.com_chanel = engin_obj.key_buffer
        self.menu_list = menu_list
        self.kind = "reader"
        
        self.menu_lengh = 1
        self.start_x = 1
        self.start_y = 1
        self.color_slot_1 = Color.ENDC
        self.color_slot_2 = Color.ENDC

        self.menu_list_draw_start = 0
        self.curser_pos = 0
        
        self.result = str()
        self.state_list = ["WIP", "ABORT", "DONE"]
        self.state = self.state_list[0]

        engin_obj.detour[0], engin_obj.detour[1] = True, self

        for entry in parameter:
            if "menu_lengh" in entry:
                 self.menu_lengh = parameter['menu_lengh']

            elif "start_vektor" in entry:
                self.start_x = parameter['start_vektor'][0]
                self.start_y = parameter['start_vektor'][1]

            elif "color_slot_1" in entry:
                 self.color_slot_1 = parameter['color_slot_1']

            elif "color_slot_2" in entry:
                 self.color_slot_2 = parameter['color_slot_2']

            else:
                pass

        self.draw()

    def change_parameter(self,  **parameter):
        for entry in parameter:
            if "menu_lengh" in entry:
                 self.menu_lengh = parameter['menu_lengh']

            elif "start_vektor" in entry:
                self.start_x = parameter['start_vektor'][0]
                self.start_y = parameter['start_vektor'][1]

            elif "color_slot_1" in entry:
                 self.color_slot_1 = parameter['color_slot_1']

            elif "color_slot_2" in entry:
                 self.color_slot_2 = parameter['color_slot_2']

            else:
                pass

        self.draw()
    
    def process(self):

        input_read = self.com_chanel.get()

        if input_read == "KEY_ESCAPE":
            self.state = self.state_list[1]
            self.engin_obj.detour = [False, None]

        elif input_read == "KEY_ENTER":
            self.result = self.menu_list[self.menu_list_draw_start + self.curser_pos]
            self.state = self.state_list[2]
            self.engin_obj.detour = [False, None]

        elif input_read == "KEY_LEFT":
            pass

        elif input_read == "KEY_RIGHT":
            pass

        elif input_read == "k" or input_read == "KEY_UP":
            self.curser_pos_old = self.curser_pos
            self.curser_pos -= 1

            if self.curser_pos < int(self.menu_lengh / 2):
                self.menu_list_draw_start -= 1
                self.curser_pos += 1

                if self.menu_list_draw_start < 0:
                    self.menu_list_draw_start += 1
                    self.curser_pos -= 1

                    if self.curser_pos < 0:
                        self.curser_pos += 1

            self.engin_obj.redraw = True
            self.draw()

        elif input_read == "j" or input_read == "KEY_DOWN":
            self.curser_pos += 1

            if self.menu_list_draw_start + self.curser_pos >= len(self.menu_list):
                self.curser_pos -= 1

            elif self.curser_pos > int(self.menu_lengh / 2):
                self.menu_list_draw_start += 1
                self.curser_pos -= 1

                if self.menu_list_draw_start + self.menu_lengh > len(self.menu_list):
                    self.menu_list_draw_start -= 1
                    self.curser_pos += 1

                if self.curser_pos >= self.menu_lengh:
                    self.curser_pos -= 1

            self.engin_obj.redraw = True
            self.draw()

    def draw(self):

        for x in range(self.menu_list_draw_start, self.menu_list_draw_start + self.menu_lengh):
            # Check if there are menu list entrys left
            # prevents list out of range error if count of entrys in menu is smaler
            # then self.menu_lengh
            if len(self.menu_list) > x:
                for i in range(len(self.menu_list[x])):
                    if x == self.curser_pos + self.menu_list_draw_start:
                        self.color_slot_1 = '\033[7m'
                        self.color_slot_2 = '\033[7m'
                    else:
                        self.color_slot_1 = '\033[0m'
                        self.color_slot_2 = '\033[0m'

                    # DO NOT draw outside FB
                    if (
                        self.start_y + x - self.menu_list_draw_start > 0 and
                        self.start_y + x - self.menu_list_draw_start < self.engin_obj.y_max and
                        self.start_x + i > 0 and
                        self.start_x + i < self.engin_obj.x_max
                    ):
                        self.engin_obj.FB[self.start_y + x - self.menu_list_draw_start][self.start_x + i] = Tile(
                            self.menu_list[x][i],
                            self.color_slot_1,
                            self.color_slot_2
                        )

            else:
                pass


class draw_noblock_menu_middle(draw_noblock_menu):

    def draw(self):

        for x in range(self.menu_list_draw_start, self.menu_list_draw_start + self.menu_lengh):
            # Check if there are menu list entrys left
            # prevents list out of range error if count of entrys in menu is smaler
            # then self.menu_lengh
            if len(self.menu_list) > x:
                for i in range(len(self.menu_list[x])):
                    if x == self.curser_pos + self.menu_list_draw_start:
                        self.color_slot_1 = '\033[7m'
                        self.color_slot_2 = '\033[7m'
                    else:
                        self.color_slot_1 = '\033[0m'
                        self.color_slot_2 = '\033[0m'

                    # DO NOT draw outside FB
                    if (
                        self.start_y + x - self.menu_list_draw_start > 0 and
                        self.start_y + x - self.menu_list_draw_start < self.engin_obj.y_max and
                        self.start_x - len(self.menu_list[x]) + i > 0 and
                        self.start_x - len(self.menu_list[x]) + i < self.engin_obj.x_max
                    ):

                        self.engin_obj.FB[self.start_y + x - self.menu_list_draw_start][self.start_x - int(len(self.menu_list[x]) / 2) + i + 1] = Tile(
                            self.menu_list[x][i],
                            self.color_slot_1,
                            self.color_slot_2
                        )

            else:
                pass


class draw_noblock_menu_right(draw_noblock_menu):

    def draw(self):

        for x in range(self.menu_list_draw_start, self.menu_list_draw_start + self.menu_lengh):
            # Check if there are menu list entrys left
            # prevents list out of range error if count of entrys in menu is smaler
            # then self.menu_lengh
            if len(self.menu_list) > x:
                for i in range(len(self.menu_list[x])):
                    if x == self.curser_pos + self.menu_list_draw_start:
                        self.color_slot_1 = '\033[7m'
                        self.color_slot_2 = '\033[7m'
                    else:
                        self.color_slot_1 = '\033[0m'
                        self.color_slot_2 = '\033[0m'

                    # DO NOT draw outside FB
                    if (
                        self.start_y + x - self.menu_list_draw_start > 0 and
                        self.start_y + x - self.menu_list_draw_start < self.engin_obj.y_max and
                        self.start_x - len(self.menu_list[x]) + i > 0 and
                        self.start_x - len(self.menu_list[x]) + i < self.engin_obj.x_max
                    ):

                        self.engin_obj.FB[self.start_y + x - self.menu_list_draw_start][self.start_x - len(self.menu_list[x]) + i] = Tile(
                            self.menu_list[x][i],
                            self.color_slot_1,
                            self.color_slot_2
                        )
            else:
                pass


# MULTISELECT MENU CLASSES
class draw_noblock_multiselect_menu:
    """
    posible parameters are:
        - menu_lengh        <---- int(),    menu lengh in lines
        - start_vektor      <---- list(),   [x, y]
        - color_slot_1      <---- str(),    forgrund collor in form of shell string
        - color_slot_2      <---- str(),    backgrund collor in form of shell string
    """

    def __init__(self, engin_obj, menu_list, **parameter):

        self.engin_obj = engin_obj
        self.com_chanel = engin_obj.key_buffer
        self.menu_list = menu_list
        self.kind = "reader"
        
        self.menu_lengh = 1
        self.start_x = 1
        self.start_y = 1
        self.color_slot_1 = Color.ENDC
        self.color_slot_2 = Color.ENDC

        self.menu_list_draw_start = 0
        self.curser_pos = 0
        
        self.result = list()
        self.selection = list()
        self.state_list = ["WIP", "ABORT", "DONE"]
        self.state = self.state_list[0]

        engin_obj.detour[0], engin_obj.detour[1] = True, self

        for entry in parameter:
            if "menu_lengh" in entry:
                 self.menu_lengh = parameter['menu_lengh']

            elif "start_vektor" in entry:
                self.start_x = parameter['start_vektor'][0]
                self.start_y = parameter['start_vektor'][1]

            elif "color_slot_1" in entry:
                 self.color_slot_1 = parameter['color_slot_1']

            elif "color_slot_2" in entry:
                 self.color_slot_2 = parameter['color_slot_2']

            else:
                pass

        self.draw()

    def change_parameter(self,  **parameter):
        for entry in parameter:
            if "menu_lengh" in entry:
                 self.menu_lengh = parameter['menu_lengh']

            elif "start_vektor" in entry:
                self.start_x = parameter['start_vektor'][0]
                self.start_y = parameter['start_vektor'][1]

            elif "color_slot_1" in entry:
                 self.color_slot_1 = parameter['color_slot_1']

            elif "color_slot_2" in entry:
                 self.color_slot_2 = parameter['color_slot_2']

            else:
                pass

        self.draw()
    
    def process(self):

        input_read = self.com_chanel.get()

        if input_read == "KEY_ESCAPE":
            self.state = self.state_list[1]
            self.engin_obj.detour = [False, None]

        elif input_read == "KEY_ENTER":
            for x in self.selection:
                self.result.append(self.menu_list[x])
            self.state = self.state_list[2]
            self.engin_obj.detour = [False, None]

        elif input_read == " ":
            tmp = self.menu_list_draw_start + self.curser_pos
            if tmp in self.selection:
                self.selection.pop(self.selection.index(tmp))
            else:
                self.selection.append(self.menu_list_draw_start + self.curser_pos)

        elif input_read == "KEY_LEFT":
            pass

        elif input_read == "KEY_RIGHT":
            pass

        elif input_read == "k" or input_read == "KEY_UP":
            self.curser_pos_old = self.curser_pos
            self.curser_pos -= 1

            if self.curser_pos < int(self.menu_lengh / 2):
                self.menu_list_draw_start -= 1
                self.curser_pos += 1

                if self.menu_list_draw_start < 0:
                    self.menu_list_draw_start += 1
                    self.curser_pos -= 1

                    if self.curser_pos < 0:
                        self.curser_pos += 1

            self.engin_obj.redraw = True
            self.draw()

        elif input_read == "j" or input_read == "KEY_DOWN":
            self.curser_pos += 1

            if self.menu_list_draw_start + self.curser_pos >= len(self.menu_list):
                self.curser_pos -= 1

            elif self.curser_pos > int(self.menu_lengh / 2):
                self.menu_list_draw_start += 1
                self.curser_pos -= 1

                if self.menu_list_draw_start + self.menu_lengh > len(self.menu_list):
                    self.menu_list_draw_start -= 1
                    self.curser_pos += 1

                if self.curser_pos >= self.menu_lengh:
                    self.curser_pos -= 1

            self.engin_obj.redraw = True
            self.draw()

    def draw(self):

        # Draw menu entris in range
        for x in range(self.menu_list_draw_start, self.menu_list_draw_start + self.menu_lengh):
            # Check if there are menu list entrys left
            # prevents list out of range error if count of entrys in menu is smaler
            # then self.menu_lengh
            if len(self.menu_list) > x:
                # Print the menu entry
                for i in range(len(self.menu_list[x])):
                    if x == self.curser_pos + self.menu_list_draw_start:
                        self.color_slot_1 = '\033[7m'
                        self.color_slot_2 = '\033[7m'

                    elif x in self.selection:
                        self.color_slot_1 = '\033[3m'
                        self.color_slot_2 = '\033[7m'

                    else:
                        self.color_slot_1 = '\033[0m'
                        self.color_slot_2 = '\033[0m'

                    # DO NOT draw outside FB
                    if (
                        self.start_y + x - self.menu_list_draw_start > 0 and
                        self.start_y + x - self.menu_list_draw_start < self.engin_obj.y_max and
                        self.start_x + i > 0 and
                        self.start_x + i < self.engin_obj.x_max
                    ):
                        self.engin_obj.FB[self.start_y + x - self.menu_list_draw_start][self.start_x + i] = Tile(
                            self.menu_list[x][i],
                            self.color_slot_1,
                            self.color_slot_2
                        )

            else:
                pass
